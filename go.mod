module RECFACE

go 1.18

require (
	github.com/Kagami/go-face v0.0.0-20210630145111-0c14797b4d0e
	gocv.io/x/gocv v0.31.0
)
