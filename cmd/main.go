package main

//
//import (
//	"fmt"
//	"github.com/dimuls/face"
//	"image"
//	"image/color"
//	"io/ioutil"
//	"log"
//	"math"
//	"path"
//	"time"
//
//	"gocv.io/x/gocv"
//)
//
//// Путь до папки с моделями. Папка должна содержать следующий файлы: dlib_face_recognition_resnet_model_v1.dat,
//// mmod_human_face_detector.dat, shape_predictor_68_face_landmarks.dat. Архивы с этими файлами можно скачать из
//// https://github.com/davisking/dlib-models.
//const modelsPath = "./models"
//
//// Путь до папки с персонами. Папка должна содержать на первом уровне папки, где название папки ― имя персоны,
//// а на втором уровне ― файлы с фотографиями лица соответствующей персоны.
//const personsPath = "./persons"
//
//// ID оборудования для получения видеопотока. В нашем случае 0 ― это ID стандартной веб-камеры.
//const deviceID = 0
//
//// Параметры векторизации, которые влияют на качество получаемого вектора:
//const (
//	padding   = 0.2 // насколько увеличивать квадрат выявленного лица;
//	jittering = 30  // кол-во генерируемых немного сдвинутых и повёрнутых копий лица.
//)
//
//// Синий цвет.
//var blue = color.RGBA{
//	R: 0,
//	G: 0,
//	B: 255,
//	A: 0,
//}
//
//// Минимальное расстояние соответствия персоне.
//const matchDistance = 0.5
//
//// Структура, описывающая персону.
//type Person struct {
//	// Имя персоны.
//	Name string
//
//	// Список дескрипторов лица персоны.
//	Descriptors []face.Descriptor
//}
//
//func main() {
//	ct := time.Now().UnixMicro()
//	// Инициализация распознавателя лиц, который будет векторизовывать лица.
//	fmt.Println("initializing face recognizer....")
//	recognizer, err := face.NewRecognizer(
//		path.Join(modelsPath, "shape_predictor_68_face_landmarks.dat"),
//		path.Join(modelsPath, "dlib_face_recognition_resnet_model_v1.dat"))
//	if err != nil {
//		log.Fatalf("create recognizer: %v", err)
//	}
//	defer recognizer.Close()
//
//	// Инициализация базы персон.
//	fmt.Println("initializing persons....")
//	persons := loadPersons(recognizer, personsPath)
//
//	// Инициализация видеопотока.
//	fmt.Println("initializing video streaming....")
//	capture, err := gocv.OpenVideoCapture("0")
//	if err != nil {
//		log.Fatalf("open video capture: %v", err)
//	}
//	defer capture.Close()
//
//	// Инициализация окна программы.
//	fmt.Println("initializing window....")
//	window := gocv.NewWindow("face-recognizer")
//	defer window.Close()
//
//	// Инициализация изображения для очередного кадра.
//	fmt.Println("initializing image frame....")
//	frame := gocv.NewMat()
//	defer frame.Close()
//
//	for {
//		// Ждём 1 миллисекунду нажатия клавиши на клавиатуре.
//		// Если нажата Esc, то выходим из приложения.
//		if window.WaitKey(1) == 27 {
//			return
//		}
//		// Пока не получим кадр продолжаем цикл.
//		if !capture.Read(&frame) {
//			continue
//		}
//		fmt.Println("detecting face......")
//		// Выявляем лица в кадре.
//		detects, err := detectFace(frame)
//		if err != nil {
//			log.Fatalf("detect faces: %v", err)
//		}
//
//		// Для каждого выявленного лица.
//
//		for _, detect := range detects {
//			fmt.Println("vectoring face......")
//			// Получаем вектор выявленного лица.
//			descriptor, err := recognizer.Recognize(frame, detect, padding, jittering)
//			if err != nil {
//				log.Fatalf("recognize face: %v", err)
//			}
//			fmt.Println("finding face......")
//			// Ищем в массиве векторов известных лиц наиболее близкое (по евклиду) лицо.
//			person, distance := findPerson(persons, descriptor)
//			fmt.Println("rectangle drawing......")
//			// Рисуем прямоугольник выявленного лица.
//			gocv.Rectangle(&frame, detect, blue, 1)
//
//			// Если расстояние между найденным известным лицом и выявленным лицом меньше
//			// какого-то порога, то пишем имя найденного известного лица над нарисованным
//			// прямоугольником.
//			if distance <= matchDistance {
//				gocv.PutText(&frame, person.Name, image.Point{
//					X: detect.Min.X,
//					Y: detect.Min.Y,
//				}, gocv.FontHersheyComplex, 1, blue, 1)
//				fmt.Println(time.Now().UnixMicro() - ct)
//				ct = time.Now().UnixMicro()
//			}
//		}
//
//		// Рисуем кадр в окне.
//		window.IMShow(frame)
//	}
//}
//
//// Вычисление евклидового расстояния.
//func euclidianDistance(a, b face.Descriptor) float64 {
//	var sum float64
//	for i := range a {
//		sum += math.Pow(float64(a[i])-float64(b[i]), 2)
//	}
//	return math.Sqrt(sum)
//}
//
//// Функция поиска наиболее близкой, заданному дескриптору, персоны.
//func findPerson(persons []Person, descriptor face.Descriptor) (Person, float64) {
//	// Объявляем переменные, которые будут хранить результаты поиска
//	var minPerson Person
//	var minDistance = math.MaxFloat64
//
//	// Проходимся по каждой персоне.
//	for _, person := range persons {
//		// Проходимся по каждому дескриптору персоны.
//		for _, personDescriptor := range person.Descriptors {
//			// Вычисляем расстояние между дескриптором персоны и заданным дескриптором.
//			distance := euclidianDistance(personDescriptor, descriptor)
//
//			// Если полученное расстояние меньше текущего минимального, то сохраняем персону и расстояние в
//			// переменные результатов.
//			if distance < minDistance {
//				minDistance = distance
//				minPerson = person
//			}
//		}
//	}
//	return minPerson, minDistance
//}
//
//// Функция загрузки базы персон.
//func loadPersons(recognizer *face.Recognizer, personsPath string) (persons []Person) {
//	// Читаем директорию, получаем массив его содержимого (информацию о файлах и папках).
//	personsDirs, err := ioutil.ReadDir(personsPath)
//	if err != nil {
//		log.Fatalf("read persons directory: %v", err)
//	}
//
//	// По каждому элементу из директории персон.
//	for _, personDir := range personsDirs {
//		// Пропускаем не директории.
//		if !personDir.IsDir() {
//			continue
//		}
//
//		// Формируем персону.
//		person := Person{
//			// Имя персоны ― название папки.
//			Name: personDir.Name(),
//		}
//
//		// Читаем директорию персоны.
//		personsFiles, err := ioutil.ReadDir(path.Join(personsPath, personDir.Name()))
//		if err != nil {
//			log.Fatalf("read person directory: %v", err)
//		}
//
//		// По каждому элементу из директории персоны.
//		for _, personFile := range personsFiles {
//			// Пропускаем если директория.
//			if personFile.IsDir() {
//				continue
//			}
//			filePath := path.Join(personsPath, personDir.Name(), personFile.Name())
//
//			// Читаем и декодируем изображение.
//			img := gocv.IMRead(filePath, gocv.IMReadUnchanged)
//
//			// Если не удалось прочитать файл и декодировать изображение, то пропускаем файл.
//			if img.Empty() {
//				continue
//			}
//
//			// Выявляем лица на изображении.
//			fmt.Println("detecting face...")
//
//			detects, err := detectFace(img)
//			if err != nil {
//				log.Fatal(err)
//			}
//			// Получаем вектор лица на изображении.
//			fmt.Println("vectoring face...")
//			descriptor, err := recognizer.Recognize(img, detects[0], padding, jittering)
//			if err != nil {
//				log.Fatalf("recognize persons face: %v", err)
//			}
//
//			// Добавляем вектор в массив векторов персоны.
//			person.Descriptors = append(person.Descriptors, descriptor)
//
//			// Освобождаем память, выделенную под изображение.
//			err = img.Close()
//			if err != nil {
//				log.Fatalf("close image: %v", err)
//			}
//		}
//
//		// Добавляем очередную персону в массив персон.
//		persons = append(persons, person)
//	}
//
//	return persons
//}
//
//func detectFace(img gocv.Mat) ([]image.Rectangle, error) {
//	classifier := gocv.NewCascadeClassifier()
//	defer classifier.Close()
//
//	if !classifier.Load("opencv_haarcascade_frontalface_default.xml") {
//		fmt.Printf("Error reading cascade file: %v\n", "opencv_haarcascade_frontalface_default.xml")
//		return nil, fmt.Errorf("Error reading cascade file: %v\n", "opencv_haarcascade_frontalface_default.xml")
//	}
//	detects := classifier.DetectMultiScale(img)
//
//	return detects, nil
//}
