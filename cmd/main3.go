package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"path"
	"path/filepath"
	"time"

	"github.com/Kagami/go-face"
)

var (
	modelsDir = "./models"
	imagesDir = "./checkPerson"
)

func main() {
	// Init the recognizer.
	rec, err := face.NewRecognizer(modelsDir)
	if err != nil {
		log.Fatalf("Can't init face recognizer: %v", err)
	}
	// Free the resources when you're finished.
	defer rec.Close()

	persons, err := loadPersons(rec, "./persons")
	if err != nil {
		log.Fatalf("Can not load persons: %s", err)
	}
	startTime := time.Now().UnixMicro()

	// Now let's try to classify some not yet known image.
	testImage := filepath.Join(imagesDir, "2022-07-15-145819.jpg")
	testFace, err := rec.RecognizeSingleFile(testImage)
	if err != nil {
		log.Fatalf("Can't recognize: %v", err)
	}
	if testFace == nil {
		log.Fatalf("Not a single face on the image")
	}
	catID := rec.ClassifyThreshold(testFace.Descriptor, 0.6)
	if catID < 0 {
		log.Fatalf("Can't classify")
	}
	// Finally print the classified label. It should be "sergey".
	fmt.Println(persons[catID])
	fmt.Println(time.Now().UnixMicro() - startTime)
}

func loadPersons(recognizer *face.Recognizer, personsPath string) ([]string, error) {
	var samples []face.Descriptor
	var cats []int32
	var persons []string
	// Читаем директорию, получаем массив его содержимого (информацию о файлах и папках).
	personsDirs, err := ioutil.ReadDir(personsPath)
	if err != nil {
		return nil, fmt.Errorf("read persons directory: %v", err)
	}

	// По каждому элементу из директории персон.
	for i, personDir := range personsDirs {
		name := personDir.Name()

		// Читаем директорию персоны. В папке пока только одна фотография
		personFile, err := ioutil.ReadDir(path.Join(personsPath, personDir.Name()))
		if err != nil {
			return nil, fmt.Errorf("read person directory: %v", err)
		}
		// По каждому элементу из директории персоны.
		filePath := path.Join(personsPath, personDir.Name(), personFile[0].Name())
		personFace, err := recognizer.RecognizeSingleFile(filePath)
		if err != nil {
			return nil, fmt.Errorf("can't recognize: %v", err)
		}
		samples = append(samples, personFace.Descriptor)
		// Each face is unique on that image so goes to its own category.
		cats = append(cats, int32(i))
		persons = append(persons, name)
	}
	recognizer.SetSamples(samples, cats)
	return persons, nil
}
